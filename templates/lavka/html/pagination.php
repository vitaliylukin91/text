<?php 
/**
* @version      4.8.0 13.08.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');

function pagination_item_active(JPaginationObject $item)
{
	return '<a' . ' href="' . $item->link . '" class="' . 'pagenav">' . $item->text . '</a>';
}

function pagination_item_inactive(JPaginationObject $item)
{
	return '<span class="pagenav">' . $item->text . '</span>';
}